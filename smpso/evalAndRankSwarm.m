function [objectiveValues, ranks] = evalAndRankSwarm(swarm, problem)
 %EVALANDRANKSWARM  Evaluate and rank the swarm's particles.

    objectiveValues = evalSwarm(swarm, problem);
    ranks = fastNonDominatedSort(objectiveValues);
end

