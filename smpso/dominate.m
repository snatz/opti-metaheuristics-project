function result = dominate(a, b, problem)
% DOMINATE  Check the domination between a and b based on the problem 
% objective values (min).
    bestIsA = 0;
    bestIsB = 0;
    
    for i = 1:problem.objCount
        valueA = problem.objectives{i}(a);
        valueB = problem.objectives{i}(b);
        
        if (valueA ~= valueB)
            if (valueA < valueB)
                bestIsA = 1;
            end
            
            if (valueB < valueA)
                bestIsB = 1;
            end
        end
    end
    
    if (bestIsA > bestIsB)
        result = -1;
    elseif (bestIsB > bestIsA)
        result = 1;
    else
        result = 0;
    end
end

