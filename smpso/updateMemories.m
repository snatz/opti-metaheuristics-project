function memories = updateMemories(memories, swarm, problem)
% UPDATEMEMORIES  Update the particles memories.

    [N, ~] = size(memories);
    
    for n = 1:N
        % If the memory does not dominate the current particle, replace!
        if (dominate(swarm(n, :), memories(n, :), problem) ~= 1)
            memories(n, :) = swarm(n, :);
        end
    end
end

