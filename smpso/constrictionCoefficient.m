function coef = constrictionCoefficient(C1, C2)
% CONSTRICTIONCOEFFICIENT  Compute the constriction coefficient.
    rho = C1 + C2;

    if (rho <= 4)
        coef = 1;
    else
        coef = 2 / (2 - rho - sqrt(rho^2 - 4*rho));
    end
end

