function swarm = initSwarm(N, vc, boundaries)
% INITSWARM  Initialize a swarm of particles between given boundaries.

    swarm = zeros(N, vc);

    for v = 1:vc
        lb = boundaries(v, 1);
        ub = boundaries(v, 2);

        swarm(:, v) = lb + rand(N, 1) .* (ub - lb);
    end
end

