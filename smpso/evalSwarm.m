function objectivesValues = evalSwarm(pop, problem)
% EVALSWARM  Evaluate the swarm for each objective and assign ranks.

    [N, ~] = size(pop);
    objectivesValues = zeros(N, problem.objCount);

    for o = 1:problem.objCount
        objectivesValues(:, o) = problem.objectives{o}(pop);
    end
end
