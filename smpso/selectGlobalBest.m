function best = selectGlobalBest(leaders, distances)
% SELECTGLOBALBEST  Select the best global particle (binary tournament based
% on crowding distance).

    [Nleaders, ~] = size(leaders);
    chosenLeaders = [randi(Nleaders) randi(Nleaders)];
    
    if (Nleaders == 1)
        best = leaders(chosenLeaders(1), :);
    elseif (distances(chosenLeaders(1)) > distances(chosenLeaders(2)))
        best = leaders(chosenLeaders(1), :);
    else
        best = leaders(chosenLeaders(2), :);
    end
    
    %[~, minIdx] = max(distances);
    %best = leaders(minIdx);
end

