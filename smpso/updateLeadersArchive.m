function [leaders, leadersValues] = updateLeadersArchive(leaders, leadersValues, ...
    swarm, problem, Narchive)
% UPDATELEADERSARCHIVE  Update the leaders archive.

    [N, ~] = size(swarm);
    [Nleaders, ~] = size(leaders);
    nonDominatedIndices = zeros(N, 1);
    leadersToRemove = zeros(Nleaders, 1);
    nonDominatedCount = 0;
    toRemoveCount = 0;

    % Keep only the non-dominated particles
    % Remove leaders whom are dominated.
    for p = 1:N
        dominated = 0;
        
        for l = 1:Nleaders
            d = dominateLeader(leadersValues(l, :), swarm(p, :), problem);
            
            if (d == -1)
                dominated = 1;
                break;
            elseif (d == 1)
                toRemoveCount = toRemoveCount + 1;
                leadersToRemove(toRemoveCount) = l;
            elseif (leaders(l, :) == swarm(p, :))
                dominated = 1;
            end
        end
        
        if (dominated == 0)
            nonDominatedCount = nonDominatedCount + 1;
            nonDominatedIndices(nonDominatedCount) = p;
        end
    end
    
    leadersToRemove = leadersToRemove(1:toRemoveCount);
    leaders(leadersToRemove, :) = [];
    leadersValues(leadersToRemove, :) = [];
    nonDominatedIndices = nonDominatedIndices(1:nonDominatedCount);
        
    % Add new leaders.
    potentialLeaders = swarm(nonDominatedIndices, :);
    [Npotential, ~] = size(potentialLeaders);
    [Nleaders, ~] = size(leaders);

    if (Narchive >= Nleaders + Npotential)
        leaders = vertcat(leaders, potentialLeaders);
        leadersValues = vertcat(leadersValues, ...
            evalSwarm(potentialLeaders, problem));
    else
        for pl = 1:Npotential
            [leaders, leadersValues] = addPotentialLeader(potentialLeaders(pl, :), ...
                leaders, leadersValues, Narchive, problem);
        end
    end
end

function [leaders, leadersValues] = addPotentialLeader(potential, leaders, ...
    leadersValues, Narchive, problem)
    
    leaders = vertcat(leaders, potential);
    leadersValues = vertcat(leadersValues, evalSwarm(potential, problem));
    [Nleaders, ~] = size(leaders);
    
    if (Nleaders > Narchive)
        distances = crowdingDistanceAssignment(leadersValues);
        [~, minIdx] = min(distances);
        leaders(minIdx, :) = [];
        leadersValues(minIdx, :) = [];
    end
end

