function [result, distancesMeans, delta] = smpso(problem, config)
% SMPSO  Execute the SMPSO algorithm.
    N = config.N;
    Gmax = config.Gmax;
    Narchive = config.Narchive;
    g = 0;
    
    % Metrics
    distancesMeans = zeros(config.Gmax, 1);
    delta = zeros(config.Gmax, 1);
    
    % Is the optimal interval available, for metric computation.
    optimalAvailable = isfield(problem, 'optimal');
    
    if (optimalAvailable)
        % Optimal solutions (for distance computation per generation)
        optimalLinspace = zeros(problem.varCount, 1000);
        optimalValues = zeros(1000, problem.objCount);

        for v = 1:problem.varCount
            optimalLinspace(v, :) = linspace(problem.optimal(v, 1), ...
                problem.optimal(v, 2), 1000);
        end

        for o = 1:problem.objCount
            optimalValues(:, o) = problem.objectives{o}(optimalLinspace');
        end
    end
    
    % Initialize the swarm of particle.
    swarm = initSwarm(N, problem.varCount, problem.boundaries);
    
    % Intialiaze the leaders archive.
    ranks = rankSwarm(swarm, problem);
    leadersIndices = ranks == 1;  % Non-dominated particles.
    leaders = swarm(leadersIndices, :);
    leadersValues = evalSwarm(leaders, problem);
    
    % Initialize the velocities.
    velocities = zeros(N, problem.varCount);
    
    % Initialize the particles memory.
    memories = swarm;
    
    while (g < Gmax)
        % Update velocities.
        velocities = updateVelocities(swarm, memories, leaders, ...
            velocities, problem, config);
        
        % Update positions.
        swarm = updatePositions(swarm, velocities, problem);
        
        % Mutation.
        swarm = polynomialMutation(swarm, config.pm, config.distIndex, ...
            problem.boundaries);
        
        % Evaluation.
        [objectiveValues, ranks] = evalAndRankSwarm(swarm, problem);
        
        % Update leaders archive.
        [leaders, leadersValues] = updateLeadersArchive(leaders, ...
            leadersValues, swarm, problem, Narchive);
        
        % Update particles memories.
        memories = updateMemories(memories, swarm, problem);
        
        % Next generation.
        g = g + 1;
        
        % Distance and delta computation.
        if (optimalAvailable)
            distances = zeros(N, 1);
            obtainedPareto = zeros(N, problem.objCount);

            for o = 1:problem.objCount
                obtainedPareto(:, o) = problem.objectives{o}(swarm);
            end
            
            % Find closest optimal values and compute distances.
            for n = 1:N
                distances(n) = min(sum((obtainedPareto(n, :) - optimalValues).^2, 2));
            end

            distancesMeans(g) = mean(sqrt(distances));
        
        	% Diversity of solutions (delta).
            delta(g) = computeDiversity(swarm(ranks == 1, :), ...
                objectiveValues(ranks == 1, :), optimalValues);
        end
    end
    
    result = leaders;
end