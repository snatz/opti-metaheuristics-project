function ranks = rankSwarm(swarm, problem)
% RANKSWARM  Evaluate the domination ranks of the swarm's particles.
    
    ranks = fastNonDominatedSort(evalSwarm(swarm, problem));
end
