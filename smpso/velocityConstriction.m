function velocities = velocityConstriction(velocities, problem)
 % VELOCITYCONSTRICTION  Velocity constriction.

    [~, vc] = size(velocities);
 
    % For each problem variable.
    deltas = (problem.boundaries(:, 2) - problem.boundaries(:, 1)) ./ 2;
    
    for v = 1:vc
        velocities(:, v) = strictNarrow(velocities(:, v), [-deltas(v) deltas(v)]);
    end
end
