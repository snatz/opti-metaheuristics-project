function result = dominateLeader(leaderValues, a, problem)
% DOMINATELEADER  Check the domination between a leader and a based on the problem 
% objective values (min).
    bestIsA = 0;
    bestIsLeader = 0;
    
    for i = 1:problem.objCount
        valueA = problem.objectives{i}(a);
        valueLeader = leaderValues(i);
        
        if (valueA ~= valueLeader)
            if (valueLeader < valueA)
                bestIsLeader = 1;
            end
            
            if (valueA < valueLeader)
                bestIsA = 1;
            end
        end
    end
    
    if (bestIsLeader > bestIsA)
        result = -1;
    elseif (bestIsA > bestIsLeader)
        result = 1;
    else
        result = 0;
    end
end

