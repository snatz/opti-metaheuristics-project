function swarm = updatePositions(swarm, velocities, problem)
% UPDATEPOSITIONS  Update the particles positions of the swarm.

    [N, vc] = size(swarm);
    
    % Update positions.
    swarm = swarm + velocities;
    
    % Check if still in the boundaries of the problem.
    % Recitfy velocities if we are not.
    for n = 1:N
        for v = 1:vc
            if (swarm(n, v) <= problem.boundaries(v, 1))
                velocities(n, v) = velocities(n, v) * -1;
            elseif (swarm(n, v) >= problem.boundaries(v, 2))
                velocities(n, v) = velocities(n, v) * -1;
            end
        end
    end
    
    for v = 1:vc
        swarm(:, v) = strictNarrow(swarm(:, v), problem.boundaries(v, :));
    end
end

