function velocities = updateVelocities(swarm, memories, leaders, ...
    velocities, problem, config)
% UPDATEVELOCITIES  Update the velocities of the swarm.

    [N, ~] = size(swarm);
    r = rand(N, 2);
    C = config.Cinterval(1) + rand(N, 2) .* (config.Cinterval(2) - config.Cinterval(1));
    distances = crowdingDistanceAssignment(evalSwarm(leaders, problem));
    
    for p = 1:N
        bestParticle = memories(p, :);
        globalBestParticle = selectGlobalBest(leaders, distances);
        
        % Update velocities.
        velocities(p, :) = constrictionCoefficient(C(p, 1), C(p, 2)) .* ...
            (config.inertiaWeight .* velocities(p, :) + ...
            C(p, 1) .* r(p, 1) .* (bestParticle - swarm(p, :)) + ...
            C(p, 2) .* r(p, 2) .* (globalBestParticle - swarm(p, :)));
    end
    
    % Velocity constriction.
    velocities = velocityConstriction(velocities, problem);
end

