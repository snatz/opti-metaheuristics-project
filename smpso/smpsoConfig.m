function config = smpsoConfig(N, Narchive, maxGen, mutProb, ...
    distIndex, Cinterval, inertiaWeight)
    % Configuration containing the population size, archive size, 
    % max number of generation, probabilities and args of mutation and SMPSO.
    config.N = N;
    config.Narchive = Narchive;
    config.Gmax = maxGen;
    config.pm = mutProb;
    config.distIndex = distIndex;
    config.Cinterval = Cinterval;
    config.inertiaWeight = inertiaWeight;
end