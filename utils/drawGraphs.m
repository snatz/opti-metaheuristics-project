function drawGraphs(problem, result, distancesMeans, delta, name, i)
    [Nsmpso, ~] = size(result);
    optimalAvailable = isfield(problem, 'optimal');
    objectivesValues = zeros(Nsmpso, problem.objCount);
    [Gmax, ~] = size(delta);
    
    if (optimalAvailable)
        optimalValues = zeros(1000, problem.objCount);
        optimalLinspace = zeros(problem.varCount, 1000);

        for v = 1:problem.varCount
            optimalLinspace(v, :) = linspace(problem.optimal(v, 1), ...
                problem.optimal(v, 2), 1000);
        end
    end
    
    for o = 1:problem.objCount
        objectivesValues(:, o) = problem.objectives{o}(result);
        
        if (isfield(problem, 'optimal'))
            optimalValues(:, o) = problem.objectives{o}(optimalLinspace');
        end
    end
    
    figure(i);
    clf;
    
    % Pareto front (objective domain)
    if (optimalAvailable)
        subplot(2, 2, [1, 2]);
    end
    
    % Result front
    plot(objectivesValues(:, 1), objectivesValues(:, 2), 'ob');
        
    title(strcat("(Objective domain) Pareto fronts for ", problem.name));
    xlabel("F_1");
    ylabel("F_2");

    if (optimalAvailable)
        hold on;
        % Optimal front
        plot(optimalValues(:, 1), optimalValues(:, 2), '-k');
        hold off;
        legend(strcat(name, " Pareto front"),'Optimal Pareto front');
        
        % Distance metric
        subplot(2, 2, 3);
        
        plot(linspace(1, Gmax, Gmax), log10(distancesMeans), '-+b');

        title(strcat("Distance metric for ", problem.name));
        xlabel("Generation");
        ylabel("Distances means (Log_{10})");
        legend(strcat(name, " distances means"));
        
        % Diversity of solutions
        subplot(2, 2, 4);
        
        plot(linspace(1, Gmax, Gmax), log10(delta), '-+b');

        title(strcat("Diversity means metric for ", problem.name));
        xlabel("Generation");
        ylabel("Delta (Log_{10})");
        legend(strcat(name, " diversity"));
    else
        legend(strcat(name, "Pareto front"));
    end
end