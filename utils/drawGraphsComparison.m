function drawGraphsComparison(problem, resultSmpso, resultMoba)
% DRAWGRAPHSCOMPARISON Compare results of SMPSO & MOBA.
    [Nsmpso, ~] = size(resultSmpso);
    [Nmoba, ~] = size(resultMoba);
    optimalAvailable = isfield(problem, 'optimal');
    objectivesValuesSmpso = zeros(Nsmpso, problem.objCount);
    objectivesValuesMoba = zeros(Nmoba, problem.objCount);
    
    if (optimalAvailable)
        optimalValues = zeros(1000, problem.objCount);
        optimalLinspace = zeros(problem.varCount, 1000);

        for v = 1:problem.varCount
            optimalLinspace(v, :) = linspace(problem.optimal(v, 1), ...
                problem.optimal(v, 2), 1000);
        end
    end
    
    for o = 1:problem.objCount
        objectivesValuesSmpso(:, o) = problem.objectives{o}(resultSmpso);
        objectivesValuesMoba(:, o) = problem.objectives{o}(resultMoba);

        if (isfield(problem, 'optimal'))
            optimalValues(:, o) = problem.objectives{o}(optimalLinspace');
        end
    end
    
    figure(1);
    clf;
    
    % Result front
    plot(objectivesValuesSmpso(:, 1), objectivesValuesSmpso(:, 2), 'ob');
        
    title(strcat("(Objective domain) Pareto fronts for ", problem.name));
    xlabel("F_1");
    ylabel("F_2");
    
    hold on;
    plot(objectivesValuesMoba(:, 1), objectivesValuesMoba(:, 2), 'or');
    
    if (optimalAvailable)
        % Optimal front
        plot(optimalValues(:, 1), optimalValues(:, 2), '-k');
        hold off;
        
        legend("SMPSO Pareto front", "MOBA Pareto front", "Optimal Pareto front");
    else
        hold off;
        legend("SMPSO Pareto front", "MOBA Pareto front");
    end
end

