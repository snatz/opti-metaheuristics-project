function resultsSmpso = benchmark(iterationCount, configSmpso, configMoba)
% BENCHMARK  Benchmark the SMPSO and the MOBA algorithms.

    % Problems
    s = sch();
    f = fon();
    z1 = zdt1();
    z2 = zdt2();
    z3 = zdt3();
    z4 = zdt4();
    z6 = zdt6();
    
    problems = [s; f; z1; z2; z3; z4; z6];
    %problems = [f];
    
    [Nproblems, ~] = size(problems);
    
    % Time, distance and diversity.
    resultsSmpso = cell(Nproblems, iterationCount, 3);
    
    resultsMoba = cell(Nproblems, iterationCount, 1);
            
    for p = 1:Nproblems
        fprintf(strcat("Problem ", problems(p).name, "\n"));
        smpsoPerf = zeros(iterationCount, 1);
        mobaPerf = zeros(iterationCount, 1);
        
        for i = 1:iterationCount
            % SMPSO
            ticSmpso = tic;
            [~, distSmpso, deltaSmpso] = smpso(problems(p), configSmpso);
            perfSmpso = toc(ticSmpso);
                        
            smpsoPerf(i) = perfSmpso;
            resultsSmpso{p, i}.performance = perfSmpso;
            resultsSmpso{p, i}.distancesMeans = distSmpso;
            resultsSmpso{p, i}.delta = deltaSmpso;
            
            fprintf(strcat("SMPSO Iteration ", int2str(i), " in ", ...
               num2str(perfSmpso), " seconds.\n"));
           
            % MOBA
            ticMoba = tic;
            moba(problems(p), configMoba);
            perfMoba = toc(ticMoba);
            
            mobaPerf(i) = perfMoba;
            resultsMoba{p, i}.performance = perfMoba;
            
            fprintf(strcat("MOBA Iteration ", int2str(i), " in ", ...
               num2str(perfMoba), " seconds.\n"));
        end
        
        fprintf(strcat("Performance average for SMPSO: ", ...
            num2str(mean(smpsoPerf)), " seconds.\n"));
        fprintf(strcat("Performance average for MOBA: ", ...
            num2str(mean(mobaPerf)), " seconds.\n"));
    end
    
    drawBenchmarkGraphs(problems, resultsSmpso, resultsMoba, iterationCount);
end

function drawBenchmarkGraphs(problems, resultsSmpso, resultsMoba, iterationCount)
    [Nproblems, ~] = size(problems);
    [Gmax, ~] = size(resultsSmpso{1, 1}.delta);
    
    perfsSmpso = zeros(iterationCount, Nproblems);
    perfsMoba = zeros(iterationCount, Nproblems);
    
    deltasSmpso = zeros(Gmax, Nproblems);
    dMeansSmpso = zeros(Gmax, Nproblems);
    
    lastGenDelta = zeros(iterationCount, Nproblems);
    lastGenDMeans = zeros(iterationCount, Nproblems);
    

    % Aggregate results.
    for p = 1:Nproblems
        for i = 1:iterationCount
            perfsSmpso(i, p) = resultsSmpso{p, i}.performance;
            perfsMoba(i, p) = resultsMoba{p, i}.performance;
        end
    end    
    
    for p =  1:Nproblems
        for g = 1:Gmax
            tempDeltas = zeros(iterationCount, Gmax);
            tempDistances = zeros(iterationCount, Gmax);
            
            for i = 1:iterationCount
                tempDeltas(i, :) = resultsSmpso{p, i}.delta;
                tempDistances(i, :) = resultsSmpso{p, i}.distancesMeans;
            end
            
           deltasSmpso(g, p) = mean(tempDeltas(:, g));
           dMeansSmpso(g, p) = mean(tempDistances(:, g));
           
           if (g == Gmax)
               lastGenDelta(:, p) = tempDeltas(:, g);
               lastGenDMeans(:, p) = tempDistances(:, g);
           end
        end
    end
    
    % Plot graphs.
    
    % Performance box plots.
    figure(1);
    
    subplot(1, 2, 1);
    boxplot(perfsSmpso);
    
    title(strcat("Performance box plot for SMPSO (", ...
        num2str(iterationCount), " iterations)"));
    xlabel("Problems");
    ylabel("Time (seconds)");
    
    subplot(1, 2, 2);
    boxplot(perfsMoba);
    
    title(strcat("Performance box plot for MOBA (", ...
        num2str(iterationCount), " iterations)"));
    xlabel("Problems");
    ylabel("Time (seconds)")
    
    for p = 1:Nproblems
        figure(p+1);

        % Diversity plot.
        subplot(1, 2, 1);

        plot(log10(deltasSmpso(:, p)));
        title(strcat("Diversity metric plot for SMPSO (", ...
            num2str(iterationCount), " iterations) - Problem ", ...
            problems(p).name));
        xlabel("Generations");
        ylabel("Delta (Log_{10})");

        % Distance plot.
        subplot(1, 2, 2);

        plot(log10(dMeansSmpso(:, p)));
        title(strcat("Distance metric plot for SMPSO (", ...
            num2str(iterationCount), " iterations) - Problem ", ...
            problems(p).name));
        xlabel("Generations");
        ylabel("Distances means (Log_{10})");
    end
end

