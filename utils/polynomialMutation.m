function pop = polynomialMutation(pop, mutateProb, distributionIndex, boundaries)
% POLYNOMIALMUTATION  Apply the polynomial mutation operation to the population.
%   The argument needed is n.

    [N, vc] = size(pop);
    
    for n = 1:N
        for v = 1:vc
            if (rand < mutateProb)
                ind = pop(n, v);
                bounds = boundaries(v, :);
                lb = bounds(1);
                ub = bounds(2);

                if (lb == ub)
                    ind = lb;
                else
                    delta1 = (ind - lb) / (ub - lb);
                    delta2 = (ub - ind) / (ub - lb);
                    r = rand();
                    mutPow = 1 / (distributionIndex + 1);

                    if (r <= 0.5)
                        xy = 1 - delta1;
                        val = 2 * r + (1 - 2 * r) * (xy)^(distributionIndex + 1);
                        deltaq = (val^mutPow) - 1;
                    else
                        xy = 1 - delta2;
                        val = 2 * (1 - r) + 2 * (r - 0.5) * (xy)^(distributionIndex + 1);
                        deltaq = 1 - (val^mutPow);
                    end

                    ind = ind + deltaq * (ub - lb);

                    if (ind < lb)
                        ind = lb;
                    elseif (ind > ub)
                        ind = ub;
                    end
                end

                pop(n, v) = ind;
            end
        end
    end
end