function weights = randomizeWeights(objCount)
    
    weights = rand(objCount, 1);
    weights = weights/(sum(weights(:, 1)));

end