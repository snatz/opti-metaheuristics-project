function somme = weightedFunction(problem, x, weights)

    somme = 0;
    for j = 1:problem.objCount
        somme = somme + problem.objectives{j}(x)*weights(j);
    end

end