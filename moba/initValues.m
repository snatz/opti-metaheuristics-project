function [bats, v, f, A, rz, r] = initValues(n, d, problem)
    
    bats = initBats(n, d, problem); %bats positions
    bats = transpose(bats);
    v = zeros(n, d);         %bats velocities
    f = zeros(n, 1);         %bats frequencies
    A = ones(n, 1)./2;       %Loudness
    rz = ones(n, 1)./2;      %Pulse rate
    r = rz;

end