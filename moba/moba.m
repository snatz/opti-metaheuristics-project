function result = moba(problem, config)
%EXECUTES THE MULTI OBJECTIVES BAT ALGORITHM (MOBA)

    %Configuration
    N = config.N;
    n = config.n;
    maxGen = config.maxGen;
    d = problem.varCount;
    objCount = problem.objCount;
    alpha = config.alpha; 
    gamma = config.gamma;
    fmin = 0;  
    fmax = 2; %Min and max frequencies are constants

    %Initialisation
    [bats, v, f, A, rz, r] = initValues(n, d, problem);
    weights = randomizeWeights(objCount);
    pareto = zeros(N, d);
    
    %Getting first best value
    for i = 1:n
        fitnesses(i) = weightedFunction(problem, bats(i, :), weights);
    end
    [fmin, I] = min(fitnesses);
    bestBat = bats(I, :);

    for j = 1:N
        for t = 1:maxGen
            for i = 1:n
                %Generate new solutions and update by (1) to (3)
                f(i) = fmin + (fmin - fmax) * rand;              
                v(i, :) = v(i, :) + (bats(i, :) - bestBat) * f(i); 
                newBats(i, :) = bats(i, :) + v(i, :);

                %Random walk around best solution
                if rand > r(i, 1)
                    newBats(i, :) = bestBat + (2 * rand - 1) * mean(A(:, 1));
                end

                %Keeping new solution in boundaries
                newBats = applyBoundaries(newBats, problem, i, d);
                
                Fnew = weightedFunction(problem, newBats(i, :), weights);
                
                if (rand < A(i, 1)) & (Fnew <= fitnesses(i))
                    %Accept the new solutions
                    bats(i, :) = newBats(i, :);
                    fitnesses(i) = Fnew;
                    %Increase ri and reduce Ai
                    A(i, 1) = A(i, 1) * alpha;
                    r(i, 1) = rz(i, 1) * (1 - exp(-1 * gamma * t));
                end

                if Fnew <= fmin
                    bestBat = newBats(i, :);
                    fmin = Fnew;
                end
                
            end
        end
        
        %Record best bat as non dominated solution
        pareto(j, :) = bestBat;
        
        %Reseting our bats
        weights = randomizeWeights(objCount);
        [bats, v, f, A, rz, r] = initValues(n, d, problem);
        for i = 1:n
            fitnesses(i) = weightedFunction(problem, bats(i, :), weights);
        end
        [fmin, I] = min(fitnesses);
        bestBat = bats(I, :);
    end

    result = pareto;

end