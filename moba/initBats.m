function bats = initBats(n, varCount, problem)

%     bats = zeros(n, varCount);
%     for i = 1:varCount
%         lb = problem.boundaries(i, 1);
%         ub = problem.boundaries(i, 2);
%         bats(:, i) = lb + rand(n, 1) .* (ub - lb);
%     end
    
     for i = 1:varCount
         lb = problem.boundaries(i, 1);
         ub = problem.boundaries(i, 2);
         bats(i, :) = lb + rand(1, n) .* (ub - lb);
     end
    
end