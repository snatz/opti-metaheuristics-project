function config = mobaConfig(n, N, maxGen, alpha, gamma)
    config.n = n;
    config.N = N;
    config.maxGen = maxGen;
    config.alpha = alpha;
    config.gamma = gamma;
end