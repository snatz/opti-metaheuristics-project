function bats = applyBoundaries(bats, problem, i, d)
    
    for c = 1:d
        lb = problem.boundaries(c, 1);
        ub = problem.boundaries(c, 2);
        if bats(i, c) < lb
            bats(i, c) = lb;
        end
        if bats(i, c) > ub
            bats(i, c) = ub;
        end                    
    end

end