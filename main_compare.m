% Add subpaths to Matlab global path. Allows global usage of GA functions.
% Only need to be executed once.
addpath(genpath('.'));

% Choose the problem and the genetic config.
problem = zdt3();

%% SMPSO
N = 100;
Narchive = 100;
Gmax = 250;
pm = 1 / problem.varCount;
%pm = 0.15;
distributionIndex = 20;
Cinterval = [1.5 2.5];
inertiaWeight = 0.1;

configSmpso = smpsoConfig(N, Narchive, Gmax, pm, distributionIndex, Cinterval, inertiaWeight);

% Execute SMPSO.
tic
[resultSmpso, ~, ~] = smpso(problem, configSmpso);
toc

%% MOBA
n = 35;
N = 35;
maxGen = 1000;
alpha = 0.9;
gamma = alpha;
configMoba = mobaConfig(n, N, maxGen, alpha, gamma);

% Execute MOBA
tic
resultMoba = moba(problem, configMoba);
toc

%% Draw graphs.
drawGraphsComparison(problem, resultSmpso, resultMoba);
