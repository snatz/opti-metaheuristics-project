## Métaheuristiques en optimisation - Projet

Simon Bar & Thomas Défossez

### Sujet

Implémentation de SMPSO (_speed-constrained multi-objective particle swarm optimization_) et de MOBA (_multi-objective bat algorithm_).
Tests/benchmark avec des problèmes connus (par exemple ZDT{1..6}).

### Structure du code

```
project/
├── moba/                       * Comprend les fichiers concernant MOBA
├── problems/                   * Comprend les fichiers concernant les différents problèmes
├── smpso/                      * Comprend les fichiers concernant SMPSO
├── utils/                      * Comprend des fonctions utilitaires
└── main.m                      * Fichier principal où le problème et la configuration des algorithmes sont définis.
```

### Configurations des algorithmes

Pour SMPSO (_smpsoConfig.m_):

* `N` correspond au nombre d'individus
* `Narchive` correspond au nombre d'individus dans l'archive des leaders
* `Gmax` correspond au nombre de générations
* `pm` correspond à la probabilité de mutation
* `distIndex` correspond à l'argument nécessaire à la mutation polynomiale
* `Cinterval` correspond à l'interval dans lequel `C_1` et `C_2` prennent leur valeur (paramètre de l'algorithme)
* `inertiaWeight` correspond à l'influence de l'inertie dans l'algorithme (paramètre de l'algorithme)

Pour MOBA (_mobaConfig.m_):
