function p = zdt4()
% ZDT4 ZDT4 problem
    p.varCount = 10;
    p.objCount = 2;
    p.objectives = {@zdt4_f1 @zdt4_f2};
    p.boundaries = [0 1; -5 5; -5 5; -5 5; -5 5; -5 5; -5 5; -5 5; -5 5; -5 5;];
    p.optimal = [0 1; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0];
    p.name = "ZDT4";
end

function o = zdt4_f1(args)
    o = args(:, 1);
end

function o = zdt4_f2(args)
    g = zdt4_g(args);
    o = g .* (1 - sqrt(args(:, 1) ./ g));
end

function o = zdt4_g(args)
    o = 1 + 10*9 + sum(args(:, 2:end).^2 - 10.*cos(4.*pi.*args(:, 2:end)), 2);
end
